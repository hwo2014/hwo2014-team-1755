package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;

import com.google.gson.GsonBuilder;
import domen.Game;
import infrastucture.*;
import infrastucture.ai.OTestAI;

public class Main {
    private static final Gson GSON = new Gson();
    private static final String TRACK_NAME = "keimola";
    private static final String PASSWORD = "secretPassword";

    public static void main(String... args) throws IOException {

        final Gson gson = new Gson();
        final ILogger logger = new LocalLogger();
        //final ILogger logger = new LocalLogger("log.txt", true);
        //final ILogger logger = new ServerLogger();

        final Game game = new Game();

        IBrain brain = new ProjectSatan(game, logger);

        final String host = args[0];
        final int port = Integer.parseInt(args[1]);
        final String botName = args[2];
        final String botKey = args[3];

        //final ILogger logger1 = new LocalLogger();
        //final ILogger logger1 = new LocalLogger("log.txt", true, "Car1");
         final ILogger logger1 = new ServerLogger();
        Game game1 = new Game();
        IBrain brain1 = new OTestAI(game1, logger1);
        String botName1 = "Car1";
        // QUICK RACE
        quickConnect(host, port, botName, botKey, logger1, brain1, game1);

        // CUSTOM RACE
        /*
         * for each bot create new Game, new Logger (to separate logs form each car and unique bot name
         * */
/*
        connectAs(host, port, botName1, botKey, logger1, brain1, game1, 3, true);

        final ILogger logger2 = new LocalLogger("log2.txt", true, "Car2");
        Game game2 = new Game();
        IBrain brain2 = new OTestAI(game2, logger2);
        String botName2 = "Car2";

        connectAs(host, port, botName2, botKey, logger2, brain2, game2, 3, false);

        final ILogger logger3 = new LocalLogger("log3.txt", true, "Car3");
        Game game3 = new Game();
        IBrain brain3 = new ProjectSatan(game3, logger3);
        String botName3 = "Car3";

        connectAs(host, port, botName3, botKey, logger3, brain3, game3, 3, false);
        */
    }

    private static void quickConnect(final String host, final int port, final String botName, final String botKey, final ILogger logger, final IBrain brain, final Game game) throws IOException {
        logger.i("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        final JoinData joinData = new JoinData(botName, botKey);
        final GameLoop gameLoop = new GameLoop(writer, reader, GSON, logger, brain, game);
        gameLoop.startRace(joinData, null);

        logger.i("End of stream, closing socket");
        writer.close();
        reader.close();
        socket.close();
    }
    

    private static void connectAs(final String host, final int port, final String botName, final String botKey, final ILogger logger, final IBrain brain, final Game game, final int carCount, final boolean createsRace) {
        (new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    logger.i("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
                    final Socket socket = new Socket(host, port);
                    final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
                    final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
                    final JoinData joinData = new JoinData(botName, botKey);
                    CreateRaceData raceData = new CreateRaceData(joinData, TRACK_NAME, PASSWORD, carCount, createsRace);

                    final GameLoop gameLoop = new GameLoop(writer, reader, GSON, logger, brain, game);
                    gameLoop.startRace(joinData, raceData);

                    logger.i("End of stream, closing socket");
                    writer.close();
                    reader.close();
                    socket.close();
                } catch (IOException ex) {
                    logger.e("IOException when connecting "+botName);
                }
            }
        })).start();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            logger.e("waiting after starting connection thread failed");
        }

    }
}