package test;

import com.google.gson.Gson;
import domen.RaceWrapper;
import domen.Turn;
import domen.car.CarId;
import domen.position.CarPositions;
import infrastucture.JoinData;
import infrastucture.LocalLogger;
import infrastucture.ai.ForecastModule;
import infrastucture.msg.ExtendedServerMessage;
import infrastucture.msg.Message;
import infrastucture.msg.MessageType;
import infrastucture.msg.SimpleMessage;

public class OTest {
    private static Gson gson = new Gson();
    public static void main(String[] arg) {
        //testJoin();
        //testPing();
        //testJoinsParsing();
        //testMyCar();
        //testRaceData();
        //testPositionData();
        //testTurn();
        //testPositionDataNoGameTick();
        testForecastModule();
    }

    private static void testJoin() {
        final JoinData joinData = new JoinData("AAAA", "BBBBB");
        String result = gson.toJson(new Message(MessageType.JOIN, joinData));
        System.out.println(result);
    }

    private static void testPing() {
        String result = gson.toJson(new SimpleMessage(MessageType.PING));
        System.out.println(result);
    }

    private static void testJoinsParsing() {
        final String data = "{\"data\":{\"name\":\"XXX\",\"key\":\"YYY\"},\"msgType\":\"joinZZZ\"}";
        final Message message = gson.fromJson(data, Message.class);
        System.out.println("Msg:"+message.toString());
        final JoinData jData = gson.fromJson(message.getDataString(), JoinData.class);
        System.out.println("JoinData:"+jData);

    }

    private static void testMyCar() {
        final String data = "{\"msgType\": \"yourCar\", \"data\": {\"name\": \"Schumacher\", \"color\": \"red\" }}";
        final Message message = gson.fromJson(data, ExtendedServerMessage.class);
        System.out.println("Msg:"+message.toString());
        final CarId carId = gson.fromJson(message.getDataString(), CarId.class);
        System.out.println("CarId:"+carId);
    }

    private static void testRaceData() {
        final String data = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"indianapolis\",\"name\":\"Indianapolis\",\"pieces\":[{\"length\":100},{\"length\":100,\"switch\":true},{\"radius\":200,\"angle\":22.5}],\"lanes\":[{\"distanceFromCenter\":-20,\"index\":0},{\"distanceFromCenter\":0,\"index\":1},{\"distanceFromCenter\":20,\"index\":2}],\"startingPoint\":{\"position\":{\"x\":-340,\"y\":-96},\"angle\":90}},\"cars\":[{\"id\":{\"name\":\"Schumacher\",\"color\":\"red\"},\"dimensions\":{\"length\":40,\"width\":20,\"guideFlagPosition\":10}},{\"id\":{\"name\":\"Rosberg\",\"color\":\"blue\"},\"dimensions\":{\"length\":40,\"width\":20,\"guideFlagPosition\":10}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":30000,\"quickRace\":true}}}}";
        final Message message = gson.fromJson(data, ExtendedServerMessage.class);
        System.out.println("Msg:"+message.toString());
        final RaceWrapper raceWrapper = gson.fromJson(message.getDataString(), RaceWrapper.class);
        System.out.println("RaceWrapper:"+raceWrapper);
    }

    private static void testPositionData() {
        final String data ="{\"msgType\":\"carPositions\",\"data\":[{\"id\":{\"name\":\"Schumacher\",\"color\":\"red\"},\"angle\":0,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":0,\"lane\":{\"startLaneIndex\":0,\"endLaneIndex\":0},\"lap\":0}},{\"id\":{\"name\":\"Rosberg\",\"color\":\"blue\"},\"angle\":45,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":20,\"lane\":{\"startLaneIndex\":1,\"endLaneIndex\":1},\"lap\":0}}],\"gameId\":\"OIUHGERJWEOI\",\"gameTick\":1}";
        final Message message = gson.fromJson(data, ExtendedServerMessage.class);
        System.out.println("Msg:"+message.toString());
        final CarPositions positions = gson.fromJson(message.getDataString(), CarPositions.class);
        System.out.println("Positions:" + positions.toString());
    }

    private static void testTurn() {
        String result = gson.toJson(new Message(MessageType.SWITCH_LANE, Turn.LEFT.getData()));
        System.out.println(result);

    }

    private static void testPositionDataNoGameTick() {
        final String data ="{\"msgType\":\"carPositions\",\"data\":[{\"id\":{\"name\":\"Schumacher\",\"color\":\"red\"},\"angle\":0,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":0,\"lane\":{\"startLaneIndex\":0,\"endLaneIndex\":0},\"lap\":0}},{\"id\":{\"name\":\"Rosberg\",\"color\":\"blue\"},\"angle\":45,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":20,\"lane\":{\"startLaneIndex\":1,\"endLaneIndex\":1},\"lap\":0}}],\"gameId\":\"OIUHGERJWEOI\"}";
        final Message message = gson.fromJson(data, ExtendedServerMessage.class);
        System.out.println("Msg:"+message.toString());
        final CarPositions positions = gson.fromJson(message.getDataString(), CarPositions.class);
        System.out.println("Positions:" + positions.toString());
    }

    private static void testForecastModule() {
        ForecastModule forecastModule = new ForecastModule(new LocalLogger());
        forecastModule.addAccelerationRecord(1, 1.5);
        forecastModule.addAccelerationRecord(2, 2.5);
        forecastModule.addAccelerationRecord(3, 3.5);
        forecastModule.addAccelerationRecord(2.5, 2.5);
        forecastModule.addAccelerationRecord(2.8, 2.5);
        forecastModule.serialize();
    }
}
