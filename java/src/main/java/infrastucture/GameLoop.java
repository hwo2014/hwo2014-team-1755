package infrastucture;

import com.google.gson.Gson;

import domen.RaceWrapper;
import domen.car.CarId;
import domen.car.Turbo;
import domen.Game;
import domen.position.CarPositions;
import infrastucture.msg.ExtendedServerMessage;
import infrastucture.msg.Message;
import infrastucture.msg.MessageType;
import infrastucture.msg.SimpleMessage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class GameLoop {
    private final PrintWriter writer;
    private final BufferedReader reader;
    private final Gson gson;
    private final ILogger logger;
    private final IBrain brain;
    private final Game game;

    public GameLoop(final PrintWriter writer, final BufferedReader reader, final Gson gson, final ILogger logger, final IBrain brain, final Game game) {
        this.writer = writer;
        this.reader = reader;
        this.gson = gson;
        this.logger = logger;
        this.brain = brain;
        this.game = game;
    }

    public void startRace(final JoinData joinData, final CreateRaceData raceData) throws IOException {
        sendJoinMessage(joinData, raceData);
        String line = null;
        while((line = reader.readLine()) != null) {
            logger.d("<--"+line);
            final ExtendedServerMessage msgFromServer = gson.fromJson(line, ExtendedServerMessage.class);
            MessageType msgType = msgFromServer.getMsgType();
            game.setGameTick(msgFromServer.getGameTick());
            if (msgType == MessageType.UNKNOWN) {
                logger.e("Unknown message type:" + msgFromServer.toString());
            } else {
                updateGameObject(msgType, msgFromServer.getDataString());
            }
            if (msgType == MessageType.GAME_INIT) {
                brain.gameInit();
            }

            if ((msgType == MessageType.CAR_POSITION || msgType == MessageType.GAME_START) && game.getGameTick() >=0) {
            	send(brain.process());
            } else if (msgType == MessageType.CRASH) {
            	logger.e("Crash");
                brain.reportCrash();
                send(IBrain.PING_MSG);
            } else if (msgType == MessageType.GAME_END) {
                brain.gameOver();
            }
        }
    }

    private void sendJoinMessage(final JoinData joinData, CreateRaceData raceData) throws IOException {
        Message msg;
        if (raceData == null) {
            msg =  new Message(MessageType.JOIN, joinData);
        } else {
            if (raceData.isCreatesNewGame()) {
                msg = new Message(MessageType.CREATE_RACE, raceData);
            } else {
                msg = new Message(MessageType.JOIN_RACE, raceData);
            }
        }
        send(msg);
    }
    private void updateGameObject(MessageType msgType, String data) {
        if (msgType == MessageType.YOUR_CAR) {
            CarId carId = gson.fromJson(data, CarId.class);
            game.setMyCar(carId);
        } else if (msgType == MessageType.GAME_INIT) {
            RaceWrapper raceWrapper = gson.fromJson(data, RaceWrapper.class);
            game.setRace(raceWrapper.getRace());
        } else if (msgType == MessageType.GAME_START) {
            // NOOP
        } else if (msgType == MessageType.CAR_POSITION) {
            CarPositions positions = gson.fromJson(data, CarPositions.class);
            game.setCarPositions(positions);
        } else if(msgType==MessageType.CRASH){
        	//game.setWaitingForSpawn(true);
        }else if(msgType == MessageType.SPAWN){
        	//game.setWaitingForSpawn(false);
        }else if(msgType == MessageType.TURBO_AVAILABLE){
        	Turbo turbo = gson.fromJson(data, Turbo.class);
        	game.setTurbo(turbo);
        }
    }

    private void send(SimpleMessage message)  throws IOException {
        String jsonMsg = gson.toJson(message);
        logger.d("-->"+jsonMsg);
        writer.println(jsonMsg);
        writer.flush();
    }
}
