package infrastucture;

/**
 * Created by User1 on 24/04/2014.
 */
public class CreateRaceData {
    private final JoinData botId;
    private final String trackName;
    private final String password;
    private final int carCount;

    private final transient boolean createsNewGame;

    public CreateRaceData(final JoinData joinData, final String trackName, final String password, final int carCount) {
        this(joinData, trackName, password, carCount, false);
    }

    public CreateRaceData(final JoinData joinData, final String trackName, final String password, final int carCount, final boolean createsNewGame) {
        this.botId = joinData;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
        this.createsNewGame = createsNewGame;
    }

    public boolean isCreatesNewGame() { return  createsNewGame; }
}
