package infrastucture;

/**
 * Created by User1 on 24/04/2014.
 */
public class JoinAnyRaceData {
    private final JoinData botId;
    private final int carCount;

    public JoinAnyRaceData(final JoinData joinData, final int carCount) {
        this.botId = joinData;
        this.carCount = carCount;
    }

}
