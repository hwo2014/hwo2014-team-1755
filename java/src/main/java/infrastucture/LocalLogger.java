package infrastucture;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Date;

public class LocalLogger implements ILogger {
	private PrintStream printStream;
    private PrintStream alternativePrintStream = null;
    private String tag = "";

    public LocalLogger() {
        printStream = System.out;
    }

	public LocalLogger(final String fileName) {
        try {
            printStream = new PrintStream(fileName);
        } catch (FileNotFoundException e) {
            System.err.println("LocalLogger: file not found, will write to default");
            printStream = System.out;
        }
    }

    public LocalLogger(final String fileName, final boolean useConsoleOutput) {
        this(fileName);
        if (useConsoleOutput && printStream != System.out) {
            alternativePrintStream = System.out;
        }
    }

    public LocalLogger(final String fileName, final boolean useConsoleOutput, String tag) {
        this(fileName, useConsoleOutput);
        this.tag = "["+tag+"]";
    }


	@Override
	public void i(String msg) {
		msg =  "|I| " + msg;
		generalWrite(msg);
	}

	@Override
	public void d(String msg) {
		msg="|D| "+msg;
		generalWrite(msg);
	}

	@Override
	public void e(String msg) {
		msg="|E| "+msg;
		generalWrite(msg);
	}

    @Override
    public void s(String msg) {
        printStream.println(msg);
        if (alternativePrintStream != null) {
            alternativePrintStream.println(msg); // raw message, without timestamp
        }
    }

    private void generalWrite(final String msg) {
    	Date date = new Date();
    	String extMessage=tag + date.toString()+" : "+msg;
        printStream.println(extMessage);
        if (alternativePrintStream != null) {
            alternativePrintStream.println(tag+msg); // raw message, without timestamp
        }
    }
}
