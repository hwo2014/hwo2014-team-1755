package infrastucture.ai;

/**
 * Created by User1 on 28/04/2014.
 */
public class TurnRecord {
    public int getTurnStartIndex() {
        return turnStartIndex;
    }

    public void setTurnStartIndex(int turnStartIndex) {
        this.turnStartIndex = turnStartIndex;
    }

    public double getTurnEntranceOptimalSpeed() {
        return turnEntranceOptimalSpeed;
    }

    public void setTurnEntranceOptimalSpeed(double turnEntranceOptimalSpeed) {
        this.turnEntranceOptimalSpeed = turnEntranceOptimalSpeed;
    }

    public boolean isWasCrash() {
        return wasCrash;
    }

    public void setWasCrash(boolean wasCrash) {
        this.wasCrash = wasCrash;
    }

    private int turnStartIndex;
    private double turnEntranceOptimalSpeed;
    private boolean wasCrash;
}
