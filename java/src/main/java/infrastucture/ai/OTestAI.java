package infrastucture.ai;

import domen.Game;
import domen.Turn;
import domen.car.CarId;
import domen.track.TrackPiece;
import infrastucture.IBrain;
import infrastucture.ILogger;
import infrastucture.msg.Message;
import infrastucture.msg.MessageType;
import infrastucture.msg.SimpleMessage;

import java.util.ArrayList;

/**
 * Created by User1 on 16/04/2014.
 */
public class OTestAI implements IBrain {
    private static final double MAX_ALLOWED_ANGLE = 55.00;
    private static final double MAX_ALLOWED_ANGLE_INCREMENT = 2;
    private static final double TURN_SPEED_ADJUST_K_AFTER_CRASH = 0.8;

    private final Game game;
    private final ILogger logger;
    private boolean isSwitchMessageSent = false;

    private double adjustedThrottle = 1.0;
    private double previousAngle = 0.0;
    private double previousSpeed = 0.0;

    private double currentTurnAngle = 0.0;
    private double currentTurnRadius = 0.0;
    private int adjustTurnAngleStep = -1;
    private TurnRecord turnRecord;

    private final ForecastModule forecastModule;

    public OTestAI(final Game game, final ILogger logger) {
        this.game = game;
        this.logger = logger;
        this.forecastModule = new ForecastModule(logger);
    }



    @Override
    public SimpleMessage process() {
        double currentSpeed = game.currentSpeed();
        double currentAngle = game.getCurrentAngle();
        logger.d("Current Speed=" + currentSpeed+";Current Angle="+ currentAngle);
        logger.d("Speed delta=" + (currentSpeed - previousSpeed)+ ";Angle delta="+(currentAngle - previousAngle));

        if (currentSpeed > previousSpeed && previousSpeed >= 0) {
            forecastModule.addAccelerationRecord(previousSpeed, currentSpeed);
        } else if (currentSpeed < previousSpeed && currentSpeed >=0 ) {
            forecastModule.addBreakingRecord(previousSpeed, currentSpeed);
        }

        TrackPiece currentTrackPiece = game.getCurrentTrackPiece();
        double nextTurnDistance = game.getDistanceToNextTurn();
        if (game.isNextTrackPieceSwitch()) {
            if (needtoOvertake()) {
                Turn turnToSendForOvertaking = game
                        .getTurnForShortestNonOptimalLane(game
                                .getClosestPieceIdxAfterSwitch(game.getMyCar()));
                if (turnToSendForOvertaking != Turn.NO_TURN) {
                    SimpleMessage ret = new Message(MessageType.SWITCH_LANE,
                            turnToSendForOvertaking.getData());
                    isSwitchMessageSent = true;
                    return ret;
                }
            }
        }

        SimpleMessage ret;
        if (game.getGameTick() == 0) {
            ret = new Message(MessageType.THROTTLE, 1.0); // full speed on first tick
        } else {
            if (needToSendTurbo()) {
                ret = new Message(MessageType.TURBO, "Whoop, whoop!");
                game.setTurbo(null);
                return ret;
            }
            if (game.isCarSwitchingLanes()) {
                isSwitchMessageSent = false;
            } else {
                if (!isSwitchMessageSent) {
                    Turn dominatingTurn = needToSwitchLanes();
                    if (dominatingTurn != Turn.NO_TURN) {
                        if (game.canShift(dominatingTurn)) {
                            ret = new Message(MessageType.SWITCH_LANE,
                                    dominatingTurn.getData());
                            isSwitchMessageSent = true;
                            return ret;
                        }
                    }
                }
            }
            Turn nextTurn = game.getNextTurn();
            if (game.canShift(nextTurn) && !isSwitchMessageSent) {
                ret = new Message(MessageType.SWITCH_LANE, nextTurn.getData());
                isSwitchMessageSent = true;
            } else {
                if (forecastModule.timeToBreakForTurn(currentSpeed, nextTurnDistance,game.getNextTurnIndex())) {
                    adjustedThrottle = 0;
                } else {
                    if (currentTrackPiece.getTurn() != Turn.NO_TURN) {
                        if (turnRecord != null && (currentAngle > MAX_ALLOWED_ANGLE || currentAngle < - MAX_ALLOWED_ANGLE)) {
                            turnRecord.setTurnEntranceOptimalSpeed(turnRecord.getTurnEntranceOptimalSpeed() * TURN_SPEED_ADJUST_K_AFTER_CRASH);
                            turnRecord.setWasCrash(true);
                        }
                        adjustedThrottle = 0;
                        if ( currentTrackPiece.isSameTurn(currentTurnAngle, currentTurnRadius)) {
                            if (adjustTurnAngleStep >=0) {
                                double deltaAngle = (currentAngle - previousAngle);
                                if (currentAngle + deltaAngle< MAX_ALLOWED_ANGLE && currentAngle +deltaAngle > -MAX_ALLOWED_ANGLE) {
                                    if (deltaAngle < MAX_ALLOWED_ANGLE_INCREMENT && deltaAngle > -MAX_ALLOWED_ANGLE_INCREMENT) {
                                        adjustedThrottle = 1;
                                        adjustTurnAngleStep++;
                                    }
                                }
                            } else if (currentAngle > previousAngle && previousAngle >= 0 || currentAngle < previousAngle && previousAngle <= 0 ||
                                    currentAngle * previousAngle <=0 && (currentAngle != 0 || previousAngle != 0)) { // angle increasing
                                adjustedThrottle = 0;
                            } else {
                                adjustTurnAngleStep = 0;
                            }
                        } else {
                            if (turnRecord != null) {
                                forecastModule.adjustTurnRecord(turnRecord, adjustTurnAngleStep);
                            }
                            adjustTurnAngleStep = -1;
                        }
                    } else {
                        adjustedThrottle = 1;
                        if (turnRecord != null) {
                            forecastModule.adjustTurnRecord(turnRecord, adjustTurnAngleStep);
                        }
                        adjustTurnAngleStep = -1;
                    }
                }
                ret = new Message(MessageType.THROTTLE, adjustedThrottle);
            }
        }

        if (currentTrackPiece.getTurn() == Turn.NO_TURN  && turnRecord != null) {
            forecastModule.addTurnRecord(turnRecord);
            turnRecord = null;
            currentTurnAngle = 0;
            currentTurnRadius = 0;
        } else if (currentTrackPiece.getTurn() != Turn.NO_TURN &&
                !currentTrackPiece.isSameTurn(currentTurnAngle, currentTurnRadius) && turnRecord != null) {
            forecastModule.addTurnRecord(turnRecord);
            turnRecord = null;
            currentTurnAngle = currentTrackPiece.getAngle();
            currentTurnRadius = currentTrackPiece.getRadius();
            turnRecord = new TurnRecord();
            turnRecord.setTurnEntranceOptimalSpeed(currentSpeed);
            turnRecord.setTurnStartIndex(game.getCurrentTrackPieceIndex());
        } else if (currentTrackPiece.getTurn() != Turn.NO_TURN && turnRecord == null) {
            turnRecord = new TurnRecord();
            turnRecord.setTurnEntranceOptimalSpeed(currentSpeed);
            turnRecord.setTurnStartIndex(game.getCurrentTrackPieceIndex());
        }
        previousAngle = currentAngle;
        previousSpeed = currentSpeed;
        return ret;
    }

    private boolean needToSendTurbo() {

        if (!game.isTurboAvailable())
            return false;
        if (!game.isOnStraightPiece())
            return false;
        if (game.getTurbo() == null)
            return false;

        double turningIn = game.getDistanceTillTurn()
                / game.currentSpeed(game.getMyCar());

        if (turningIn > game.getTurbo().getTurboDurationTicks()
                * game.getTurbo().getTurboFactor())
            return true;
        return false;
    }

    private Turn needToSwitchLanes() {
        double leftLength = game.getNextSegmentLength(Turn.LEFT);
        double rightLength = game.getNextSegmentLength(Turn.RIGHT);
        if (leftLength > rightLength)
            return Turn.LEFT;
        else if (rightLength > leftLength)
            return Turn.RIGHT;
        return Turn.NO_TURN;
    }

    private boolean needtoOvertake() {
        ArrayList<CarId> possibleOvertakingTargets = game
                .getPossibleOvertakingTargets();
        int secondSwitchFromMe = game.getSecondSwitchPieceId(game
                .getCurrentTrackPieceIndex());
        double myDistanceToSwitch = game.getDistance(secondSwitchFromMe, false,
                game.getMyCar());
        double mySpeed = game.currentSpeed();
        for (CarId carId : possibleOvertakingTargets) {
            if (game.getDistance(secondSwitchFromMe, true, carId)
                    / game.currentSpeed(carId) > myDistanceToSwitch / mySpeed) {
                logger.d("Overtaking carID: " + carId + " On piece:"
                        + game.getCurrentTrackPieceIndex());
                return true;
            }
        }
        return false;
    }

    @Override
    public void gameOver() {
        forecastModule.serialize();
    }

    @Override
    public void gameInit() {

    }

    @Override
    public void reportCrash() {
        if (turnRecord != null) {
            turnRecord.setTurnEntranceOptimalSpeed(turnRecord.getTurnEntranceOptimalSpeed() * TURN_SPEED_ADJUST_K_AFTER_CRASH);
            turnRecord.setWasCrash(true);
        }
    }
}
