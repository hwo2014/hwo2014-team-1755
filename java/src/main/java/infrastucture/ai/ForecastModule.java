package infrastucture.ai;

import com.google.gson.Gson;
import infrastucture.ILogger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ForecastModule {
    private static final double ACC_DEFAULT = 0.19; // for ACC use larger speed diff
    private static final double BREAK_DEFAULT = 0.1; // for BREAK use smaller speed diff

    private final ILogger logger;
    private List<AccelerationRecord> acceleration = new ArrayList<AccelerationRecord>(); // by currentSpeed
    private List<AccelerationRecord> breaking = new ArrayList<AccelerationRecord>(); // by nextSpeed
    private List<TurnRecord> turnRecords = new ArrayList<TurnRecord>(); // turn records

    public ForecastModule(final ILogger logger) {
        this.logger = logger;
        load();
    }

    public TurnRecord getTurnRecord(int index) {
        for(TurnRecord record : turnRecords) {
           if (record != null && record.getTurnStartIndex() == index) {
               return record;
           }
        }
        return null;
    }


    public boolean timeToBreakForTurn(double startSpeed, double distance, int nextTurnIndex) {
        TurnRecord record = getTurnRecord(nextTurnIndex);
        double defaultOptimalSpeed = 6.1;
        if (record != null) {
            defaultOptimalSpeed = record.getTurnEntranceOptimalSpeed();
        }
        return timeToBreak(startSpeed, distance, defaultOptimalSpeed);
    }
    public boolean timeToBreak(double startSpeed, double distance, double expectedSpeed) {
        if (startSpeed < expectedSpeed) {
            return false;
        } else {
            int index;
            double currentSpeed = expectedSpeed;
            double distanceLeft = distance;
            while(distanceLeft > 0 && currentSpeed < startSpeed) {
                index = findIndexClosest(0, breaking.size() - 1, breaking, currentSpeed, true);
                if (index != -1) {
                    AccelerationRecord record = breaking.get(index);
                    if (Math.abs(currentSpeed - record.getNextSpeed()) < 0.01) {
                        currentSpeed = record.getCurrentSpeed();
                    } else {
                        currentSpeed = currentSpeed + BREAK_DEFAULT;
                    }
                } else {
                    currentSpeed = currentSpeed + BREAK_DEFAULT;
                }
                distanceLeft -= currentSpeed;
            }
            return distanceLeft < 0;
        }
    }

    public void addAccelerationRecord(final double previousSpeed, final double nextSpeed) {
        int index = findIndexClosest(0, acceleration.size()-1, acceleration, previousSpeed, false);
        AccelerationRecord recordToAdd = new AccelerationRecord(previousSpeed, 1, nextSpeed, false);
        if (index == -1) {
            acceleration.add(recordToAdd);
        } else {
            AccelerationRecord record = acceleration.get(index);
            if (record.getCurrentSpeed() == previousSpeed) {
                acceleration.add(index + 1, recordToAdd);
                //logger.d("Removed record=" + record);
                acceleration.remove(index);
            }else if (record.getCurrentSpeed() < previousSpeed) {
                acceleration.add(index + 1, recordToAdd);
            } else {
                acceleration.add(index, recordToAdd);
            }
        }
    }

    public void addBreakingRecord(final double previousSpeed, final double nextSpeed) {
        int index = findIndexClosest(0, breaking.size()-1, breaking, nextSpeed, true);
        AccelerationRecord recordToAdd = new AccelerationRecord(previousSpeed, 0, nextSpeed, false);
        if (index == -1) {
            breaking.add(recordToAdd);
        } else {
            AccelerationRecord record = breaking.get(index);
            if (record.getNextSpeed() == previousSpeed) {
                breaking.add(index + 1, recordToAdd);
                logger.d("Removed record=" + record);
                breaking.remove(index);
            }else if (record.getNextSpeed() < previousSpeed) {
                breaking.add(index + 1, recordToAdd);
            } else {
                breaking.add(index, recordToAdd);
            }
        }
    }

    //0public void

    public void serialize() {
        Gson gson = new Gson();
        logger.s("====SERIALIZATION====");
        logger.s("ACCELERATION");
        for (AccelerationRecord record : acceleration) {
            logger.s(gson.toJson(record));
        }
        logger.s("BREAKING");
        for (AccelerationRecord record : breaking) {
            logger.s(gson.toJson(record));
        }
        logger.s("TURNS");
        for (TurnRecord record : turnRecords) {
            logger.s(gson.toJson(record));
        }
        logger.s("====SERIALIZATION END====");
    }

    private static int findIndexClosest(int startIndex, int endIndex, List<AccelerationRecord> list, double speed, boolean useNextSpeed) {
        if (startIndex > endIndex) {
            return -1;
        } else if (startIndex == endIndex) {
            return startIndex;
        } else {
            int middle = (startIndex + endIndex) / 2;
            AccelerationRecord record = list.get(middle);
            double speedToCompare = useNextSpeed ? record.getNextSpeed() : record.getCurrentSpeed();
            if (speedToCompare >= speed) {
                return findIndexClosest(startIndex, middle, list, speed, useNextSpeed);
            } else {
                return findIndexClosest(middle+1, endIndex, list, speed, useNextSpeed);
            }
        }
    }

    private final void load() {
        try {
            Gson gson = new Gson();
            BufferedReader br = new BufferedReader(new FileReader(new File("speedprofile.dat")));
            String line;
            boolean isAcceleration = false;
            boolean isBreaking = false;
            boolean isTurns = false;
            while ((line = br.readLine()) != null) {
                if ("".equals(line.trim()) ) {
                    // NOOP
                } else if ("PROFILE".equals(line)) {
                    // NOOP
                } else if ("ACCELERATION".equals(line)) {
                    isAcceleration = true;
                    isBreaking = false;
                    isTurns = false;
                } else if ("BREAKING".equals(line)) {
                    isAcceleration = false;
                    isBreaking = true;
                    isTurns = false;
                } else if ("TURNS".equals(line)) {
                    isAcceleration = false;
                    isBreaking = false;
                    isTurns = true;
                } else if (isAcceleration) {
                    AccelerationRecord record = gson.fromJson(line, AccelerationRecord.class);
                    record.setFromProfile();
                    acceleration.add(record);
                } else if (isBreaking) {
                    AccelerationRecord record = gson.fromJson(line, AccelerationRecord.class);
                    record.setFromProfile();
                    breaking.add(record);
                } else if (isTurns) {
                    TurnRecord record = gson.fromJson(line, TurnRecord.class);
                    turnRecords.add(record);
                }
            }
        } catch (FileNotFoundException e) {
            logger.e("speed profile file not loaded");
        } catch (IOException e) {
            logger.e("cant read profile file");
        }
    }

    public void addTurnRecord(TurnRecord turnRecord) {
        TurnRecord old = getTurnRecord(turnRecord.getTurnStartIndex());
        if (old == null) {
            turnRecords.add(turnRecord);
        } else {
            if (old.isWasCrash() && !turnRecord.isWasCrash()) {
                turnRecords.remove(old);
                turnRecords.add(turnRecord);
            } else if (old.isWasCrash() && turnRecord.isWasCrash() && turnRecord.getTurnEntranceOptimalSpeed() < old.getTurnEntranceOptimalSpeed()){
                turnRecords.remove(old);
                turnRecords.add(turnRecord);
            } else if (!old.isWasCrash() && !turnRecord.isWasCrash() && turnRecord.getTurnEntranceOptimalSpeed() > old.getTurnEntranceOptimalSpeed()) {
                turnRecords.remove(old);
                turnRecords.add(turnRecord);
            } else {
                old.setTurnEntranceOptimalSpeed(old.getTurnEntranceOptimalSpeed() * 0.9);
            }
        }
    }

    public void adjustTurnRecord(TurnRecord turnRecord, int stepsUp) {
        if (turnRecord.isWasCrash()) {
            return;
        }
        int timesToIncreaseOptimalSpeed = stepsUp / 3;
        double speed = turnRecord.getTurnEntranceOptimalSpeed();
        for (int i = 0 ; i < timesToIncreaseOptimalSpeed; i++) {
            int index = findIndexClosest(0, acceleration.size() - 1, acceleration, speed, false);
            if (index == -1) {
                break;
            } else {
                speed = acceleration.get(index).getNextSpeed();
            }

        }
        turnRecord.setTurnEntranceOptimalSpeed(speed);
    }
}
