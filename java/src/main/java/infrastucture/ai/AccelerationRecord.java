package infrastucture.ai;

/**
 * Created by User1 on 25/04/2014.
 */
public class AccelerationRecord {
    private double currentSpeed;
    private double throttle;
    private double nextSpeed;
    private boolean isForecast;

    public AccelerationRecord(double currentSpeed, double throttle, double nextSpeed, boolean isForecast) {
        this.currentSpeed = currentSpeed;
        this.throttle = throttle;
        this.nextSpeed = nextSpeed;
        this.isForecast = isForecast;
    }

    public double getCurrentSpeed() { return currentSpeed; }
    public double getNextSpeed() { return nextSpeed; }

    public void setFromProfile() { isForecast = true; }
    @Override
    public String toString() {
        return "currentSpeed="+currentSpeed+";throttle="+throttle+";nextSpeed="+nextSpeed+";isForecast="+isForecast;
    }
}
