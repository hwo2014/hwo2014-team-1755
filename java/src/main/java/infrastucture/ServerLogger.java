package infrastucture;

public class ServerLogger implements ILogger {
	@Override
	public void i(String msg) {
		System.out.println(msg);
	}

	@Override
	public void d(String msg) {
        // NOOP
	}

    @Override
    public void s(String msg) {
        // NOOP
    }

    @Override
	public void e(String msg) {
		System.out.println(msg);
	}

}
