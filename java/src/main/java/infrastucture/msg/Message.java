package infrastucture.msg;

/**
 * Created by User1 on 15/04/2014.
 */
public class Message extends SimpleMessage {
    private final Object data;

    public Message() {
        super();
        this.data = null;
    }

    public Message(final MessageType msgType, final Object data) {
        super(msgType);
        this.data = data;
    }

    public String getDataString() {
        return data == null ? "" : data.toString();
    }

    @Override
    public String toString() {
        return super.toString()+";data=["+data+"]";
    }
}
