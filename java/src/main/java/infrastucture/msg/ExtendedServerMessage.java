package infrastucture.msg;

public class ExtendedServerMessage extends Message {
    private final String gameId;
    private final long gameTick;

    public ExtendedServerMessage() {
        super();
        this.gameId = null;
        this.gameTick = -1;
    }

    public ExtendedServerMessage(final MessageType messageType, final Object data, final String gameId, final long gameTick) {
        super(messageType, data);
        this.gameId = gameId;
        this.gameTick = gameTick;
    }

    public long getGameTick() { return gameTick; }
    @Override
    public String toString() {
        return super.toString() + ";gameId="+gameId+";gameTick="+gameTick;
    }
}
