package infrastucture.msg;

/**
 * Created by User1 on 15/04/2014.
 */
public enum MessageType {
    CREATE_RACE("createRace"),
    JOIN_RACE("joinRace"),
	LAP_FINISHED("lapFinished"),
	TURBO("turbo"),
	TURBO_AVAILABLE("turboAvailable"),
	SPAWN("spawn"),
	CRASH("crash"),
	SWITCH_LANE("switchLane"),
    UNKNOWN(""),
    JOIN("join"),
    PING("ping"),
    YOUR_CAR("yourCar"),
    CAR_POSITION("carPositions"),
    THROTTLE("throttle"),
    GAME_START("gameStart"),
    GAME_END("gameEnd"),
    GAME_INIT("gameInit");

    private final String key;

    private MessageType(final String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public static MessageType formString(String messageType) {
        for(MessageType type: values()) {
            if (type.getKey().equals(messageType)) {
                return type;
            }
        }
        return UNKNOWN;
    }
}
