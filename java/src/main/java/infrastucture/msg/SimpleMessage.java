package infrastucture.msg;

public class SimpleMessage {
    private final String msgType;

    public SimpleMessage() {
        this.msgType = MessageType.UNKNOWN.getKey();
    }

    public SimpleMessage(final MessageType msgType) {
        this.msgType = msgType.getKey();
    }

    public MessageType getMsgType() {
        return MessageType.formString(msgType);
    }

    @Override
    public String toString() {
        return "msgType="+msgType;
    }
}
