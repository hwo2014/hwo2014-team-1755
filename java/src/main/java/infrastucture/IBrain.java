package infrastucture;

import infrastucture.msg.MessageType;
import infrastucture.msg.SimpleMessage;

/**
 * Created by User1 on 15/04/2014.
 */
public interface IBrain {
    public static final SimpleMessage PING_MSG = new SimpleMessage(MessageType.PING);
    SimpleMessage process();
    void gameOver();
    void gameInit();

    void reportCrash();
}
