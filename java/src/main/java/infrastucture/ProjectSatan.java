package infrastucture;

import java.util.ArrayList;

import infrastucture.msg.Message;
import infrastucture.msg.MessageType;
import infrastucture.msg.SimpleMessage;
import domen.Game;
import domen.Turn;
import domen.car.CarId;

public class ProjectSatan implements IBrain {
	private final Game game;
	private final ILogger logger;
	private double throttleOnTurn = 0.65;
	private double throtlleOnStraight = 1.0;
	public double throtlleToBrake = 0.42;
	private double emergencyBrake = 0.0;
	private double safeSlideAngle = 55.0;
	private boolean isSwitchMessageSent = false;
	private int ticsToBrake = 20;

	public double speedToEnterTurn = 6.4;

	public ProjectSatan(final Game game, final ILogger logger) {
		this.game = game;
		this.logger = logger;
	}

	@Override
	public SimpleMessage process() {
		if (game.isNextTrackPieceSwitch()) {
			if (needtoOvertake()) {
				Turn turnToSendForOvertaking = game
						.getTurnForShortestNonOptimalLane(game
								.getClosestPieceIdxAfterSwitch(game.getMyCar()));
				if (turnToSendForOvertaking != Turn.NO_TURN) {
					SimpleMessage ret = new Message(MessageType.SWITCH_LANE,
							turnToSendForOvertaking.getData());
					isSwitchMessageSent = true;
					return ret;
				}
			}
		}

		SimpleMessage ret;
		if (game.getGameTick() == 0) {
			ret = new Message(MessageType.THROTTLE, 1.0); // full speed on first
															// tick
		} else {
			if (needToSendTurbo()) {
				ret = new Message(MessageType.TURBO, "Whoop, whoop!");
				game.setTurbo(null);
				return ret;
			}
			if (game.isCarSwitchingLanes()) {
				isSwitchMessageSent = false;
			} else {
				if (!isSwitchMessageSent) {
					Turn dominatingTurn = needToSwitchLanes();
					if (dominatingTurn != Turn.NO_TURN) {
						if (game.canShift(dominatingTurn)) {
							ret = new Message(MessageType.SWITCH_LANE,
									dominatingTurn.getData());
							isSwitchMessageSent = true;
							return ret;
						}
					}
				}
			}
			double throtlleToSend = calculateThrotlleToBeSet();
			ret = new Message(MessageType.THROTTLE, throtlleToSend);
		}
		return ret;
	}

	private Turn needToSwitchLanes() {
		double leftLength = game.getNextSegmentLength(Turn.LEFT);
		double rightLength = game.getNextSegmentLength(Turn.RIGHT);
		if (leftLength > rightLength)
			return Turn.LEFT;
		else if (rightLength > leftLength)
			return Turn.RIGHT;
		return Turn.NO_TURN;
	}

	private boolean needtoOvertake() {
		ArrayList<CarId> possibleOvertakingTargets = game
				.getPossibleOvertakingTargets();
		int secondSwitchFromMe = game.getSecondSwitchPieceId(game
				.getCurrentTrackPieceIndex());
		double myDistanceToSwitch = game.getDistance(secondSwitchFromMe, false,
				game.getMyCar());
		double mySpeed = game.currentSpeed();
		for (CarId carId : possibleOvertakingTargets) {
			if (game.getDistance(secondSwitchFromMe, true, carId)
					/ game.currentSpeed(carId) > myDistanceToSwitch / mySpeed) {
				logger.d("Overtaking carID: " + carId + " On piece:"
						+ game.getCurrentTrackPieceIndex());
				return true;
			}
		}
		return false;
	}

	private boolean needToSendTurbo() {

		if (!game.isTurboAvailable())
			return false;
		if (!game.isOnStraightPiece())
			return false;
		if (game.getTurbo() == null)
			return false;

		double turningIn = game.getDistanceTillTurn()
				/ game.currentSpeed(game.getMyCar());

		if (turningIn > game.getTurbo().getTurboDurationTicks()
				* game.getTurbo().getTurboFactor())
			return true;
		return false;
	}

	private double calculateThrotlleToBeSet() {
		if (game.isOnStraightPiece()) {
			double turningIn = game.getDistanceTillTurn()
					/ game.currentSpeed(game.getMyCar());
			if (turningIn > ticsToBrake) {
				return throtlleOnStraight;
			} else {
				return throtlleToBrake;
			}

		} else {
			double currentSpeed = game.currentSpeed();
			double currentAngleIncrement = game.getAngleIncrementSpeed();

			double ticsTillEndSegment = Math.round(game
					.getCurrentElementLength() / game.currentSpeed());
			double ticsTillCrash = Math.round((safeSlideAngle - Math.abs(game
					.getCurrentAngle()))
					/ (game.getAngleIncrementSpeed() + game
							.getAngleIncrementAcceleration()));

			double angleIncrementPropolsion = game
					.getAngleIncrementAcceleration();

			double throttleToreturn = 0.0;

			logger.d("Tics till end Segment:" + ticsTillEndSegment);
			logger.d("Tics till crash:" + ticsTillCrash);
			logger.d("Current Angle: " + game.getCurrentAngle());
			logger.d("Current Angle Increment:" + game.getAngleIncrementSpeed());
			logger.d("Current Angle Increment Acceleration:"
					+ angleIncrementPropolsion);

			if (currentSpeed == 0) {// Probbably waiting for restore after crash
				throttleToreturn = throtlleOnStraight;
			} else if (currentAngleIncrement == 0) {
				throttleToreturn = throtlleOnStraight;
			} else if (ticsTillCrash - 10 <= ticsTillEndSegment) {// possible
																	// emergency

				if (angleIncrementPropolsion > 0) {
					throttleToreturn = emergencyBrake;
				} else {
					throttleToreturn = throtlleToBrake;

				}
			} else if (ticsTillCrash - 10 > ticsTillEndSegment) {
				throttleToreturn = throtlleOnStraight;
			}

			logger.d("Piece ID: " + game.getCurrentTrackPieceIndex()
					+ ". Turning, throttle to Set: " + throttleToreturn
					+ " Current speed: " + game.currentSpeed());
			return throttleToreturn;
			/*
			 * if(game.getCurrentAngle() > safeSlideAngle){
			 * if(15.0*game.getAngleIncrementSpeed
			 * ()+game.getCurrentAngle()>=60.0){
			 * logger.d("Emergency Brake on Turn"); return emergencyBrake; }
			 * logger.d("Piece ID: " + game.getCurrentTrackPieceIndex()+
			 * ". Sliding to much, throttle to Set: " +
			 * throtlleToBrake+" Current speed: " +
			 * game.currentSpeed(game.getMyCar())); return throtlleToBrake; }
			 * logger.d("Piece ID: " +
			 * game.getCurrentTrackPieceIndex()+". Turning, throttle to Set: " +
			 * throttleOnTurn+" Current speed: " +
			 * game.currentSpeed(game.getMyCar())); return throttleOnTurn;
			 */
		}
	}

	@Override
	public void gameOver() {

    }

    @Override
    public void gameInit() {

    }

    @Override
    public void reportCrash() {

    }
}
