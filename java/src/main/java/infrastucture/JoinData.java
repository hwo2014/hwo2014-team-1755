package infrastucture;

// TODO: move to domain
public class JoinData {
    private final String name;
    private final String key;

    public JoinData(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    public String toString() {
        return "name="+name+";key="+key;
    }
}
