package infrastucture;

public interface ILogger {
    public void i(String msg);
    public void d(String msg);
    public void e(String msg);
    public void s(String msg);
}
