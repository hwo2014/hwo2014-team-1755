package domen.position;

public class PiecePosition {
    private final int pieceIndex;
    private final double inPieceDistance;
    private final LanePosition lane;
    private final int lap;

    public PiecePosition(final int pieceIndex, final double inPieceDistance, final LanePosition lanePosition, final int lap) {
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
        this.lane = lanePosition;
        this.lap = lap;
    }

    @Override
    public String toString() {
        return "pieceIndex="+pieceIndex+";inPieceDistance="+inPieceDistance+";lanePosition=["+lane.toString()+"];lap="+lap;
    }

	public LanePosition getLane() {
		return lane;
	}

	public int getPieceIndex() {
		return pieceIndex;
	}


    public double getInPieceDistance() {
        return inPieceDistance;
    }

	public int getLap() {
		return lap;
	}

}
