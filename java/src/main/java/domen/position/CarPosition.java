package domen.position;

import domen.car.CarId;

public class CarPosition {
    private final CarId id;
    private final double angle;
    private final PiecePosition piecePosition;

    public CarPosition(final CarId carId, final double angle, final PiecePosition piecePosition) {
        this.id = carId;
        this.angle = angle;
        this.piecePosition = piecePosition;
    }

    @Override
    public String toString() {
        return "id=["+id+"];angle="+angle+";piecePosition=["+piecePosition+"]";
    }

    public CarId getCarId() {
        return id;
    }

    public double getAngle() { return angle; }

	public PiecePosition getPiecePosition() {
		return piecePosition;
	}
    
}
