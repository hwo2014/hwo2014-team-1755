package domen.position;

public class LanePosition {
    private final int startLaneIndex;
    private final int endLaneIndex;

    public LanePosition(final int startLaneIndex, final int endLaneIndex) {
        this.startLaneIndex = startLaneIndex;
        this.endLaneIndex = endLaneIndex;
    }

    public boolean isSwitching() {
        return startLaneIndex != endLaneIndex;
    }

    @Override
    public String toString() {
        return "startLane="+startLaneIndex+";endLane="+endLaneIndex;
    }

	public int getEndLaneIndex() {
		return endLaneIndex;
	}

	public int getStartLaneIndex() {
		return startLaneIndex;
	}
}
