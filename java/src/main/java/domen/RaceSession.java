package domen;

/**
 * Created by User1 on 16/04/2014.
 */
public class RaceSession {
    private final int laps;
    private final long maxLapTimeMs;
    private final boolean quickRace;

    public RaceSession(final int laps, final long maxLapTimeMs, final boolean quickRace) {
        this.laps = laps;
        this.maxLapTimeMs = maxLapTimeMs;
        this.quickRace = quickRace;
    }

    @Override
    public String toString() {
        return "laps="+laps+";maxLapTimeMs="+maxLapTimeMs+";quickRace="+quickRace;
    }
}
