package domen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import domen.car.CarId;
import domen.car.Turbo;
import domen.position.CarPosition;
import domen.position.CarPositions;
import domen.position.LanePosition;
import domen.position.PiecePosition;
import domen.track.Lane;
import domen.track.Track;
import domen.track.TrackPiece;

public class Game {
	private CarId myCar;
	// TODO: Archive (history records) by tick
	private Map<CarId, CarPosition> previousCarPositions = new HashMap<CarId, CarPosition>();
	private Map<CarId, CarPosition> currentCarPositions = new HashMap<CarId, CarPosition>();

	private Race race;
	private long gameTick = -1;
	private Turbo turbo;

	private double previousSpeed = 0.0;
	private double previousAngleSpeed = 0.0;

	public void setMyCar(final CarId myCar) {
		this.myCar = myCar;
	}

	public void setRace(final Race race) {
		this.race = race;
	}

	public void setCarPositions(CarPositions carPositions) {
		previousSpeed = currentSpeed();
		previousAngleSpeed = getAngleIncrementSpeed();
		previousCarPositions.clear();
		previousCarPositions.putAll(currentCarPositions);
		currentCarPositions.clear();
		for (CarPosition position : carPositions) {
			currentCarPositions.put(position.getCarId(), position);
		}
	}

	public long getGameTick() {
		return gameTick;
	}

	public void setGameTick(long gameTick) {
		if (gameTick >= 0) {
			this.gameTick = gameTick;
		}
	}

	public double currentSpeed() {
		return currentSpeed(myCar);
	}

	public double currentSpeed(CarId carId) {
		double currentSpeed = 0.0;
		CarPosition prevPosition = previousCarPositions.get(carId);
		if (prevPosition != null) {
			PiecePosition previousPiecePosition = prevPosition
					.getPiecePosition();
			PiecePosition currentPiecePosition = currentCarPositions.get(carId)
					.getPiecePosition();
			if (previousPiecePosition.getPieceIndex() == currentPiecePosition
					.getPieceIndex()) {
				currentSpeed = currentPiecePosition.getInPieceDistance()
						- previousPiecePosition.getInPieceDistance();
			} else {
				Track track = race.getTrack();
				Lane currentLane = track.getLaneByIndex(currentPiecePosition
						.getLane().getStartLaneIndex());
				int distanceFormCenter = currentLane.getDistanceFromCenter();
				double previousLaneLength;
				if (previousPiecePosition.getLane().isSwitching()) { // TODO:
																		// not
																		// working
																		// if
																		// switch
																		// is on
																		// bend
					double distanceBetweenLanes = Math.abs(track
							.getLaneByIndex(
									previousPiecePosition.getLane()
											.getStartLaneIndex())
							.getDistanceFromCenter()
							- track.getLaneByIndex(
									previousPiecePosition.getLane()
											.getEndLaneIndex())
									.getDistanceFromCenter());
					previousLaneLength = track
							.getPieces()
							.get(prevPosition.getPiecePosition()
									.getPieceIndex())
							.getSwitchingLength(distanceBetweenLanes);
				} else {
					previousLaneLength = track
							.getPieces()
							.get(prevPosition.getPiecePosition()
									.getPieceIndex())
							.getLength(distanceFormCenter);
				}
				currentSpeed = previousLaneLength
						- prevPosition.getPiecePosition().getInPieceDistance()
						+ currentPiecePosition.getInPieceDistance();
			}
		}
		return currentSpeed;
	}

	public boolean canShift(Turn turn) {
		if (turn == Turn.RIGHT) {
			return currentCarPositions.get(myCar).getPiecePosition().getLane()
					.getEndLaneIndex() != race.getTrack().getRightmostLane();
		} else if (turn == Turn.LEFT) {
			return currentCarPositions.get(myCar).getPiecePosition().getLane()
					.getEndLaneIndex() != race.getTrack().getLeftmostLane();
		}
		return false;
	}

	public boolean isCarSwitchingLanes() {
		return currentCarPositions.get(myCar).getPiecePosition().getLane()
				.getEndLaneIndex() != currentCarPositions.get(myCar)
				.getPiecePosition().getLane().getStartLaneIndex();
	}

	public CarId getMyCar() {
		return myCar;
	}

	public double getCurrentAngle() {
		return currentCarPositions.get(myCar).getAngle();
	}

	public TrackPiece getCurrentTrackPiece() {
		return race.getTrack().getPieces().get(getCurrentTrackPieceIndex());
	}

	public Turn getNextTurn() {
		int currentPiece = currentCarPositions.get(myCar).getPiecePosition()
				.getPieceIndex();
		TrackPiece nextTrackPiece = getNextTrackPiece(currentPiece);
		double currentPieceAngle = race
				.getTrack()
				.getPieces()
				.get(currentCarPositions.get(myCar).getPiecePosition()
						.getPieceIndex()).getAngle();
		if (currentPieceAngle == 0) {// currently on straight road.
			while (nextTrackPiece.getAngle() == 0.0) {
				if (currentPiece == race.getTrack().getPieces().size() - 1) {
					currentPiece = 0;
				} else {
					currentPiece++;
				}
				nextTrackPiece = getNextTrackPiece(currentPiece);
			}
			if (nextTrackPiece.getAngle() < 0) {
				return Turn.LEFT;
			} else {
				return Turn.RIGHT;
			}
		} else {// currently turning
			boolean isOnRightTurn = currentPieceAngle > 0;
			while (nextTrackPiece.getAngle() > 0 == isOnRightTurn) {
				if (nextTrackPiece.getAngle() == 0)
					break;
				if (currentPiece == race.getTrack().getPieces().size() - 1) {
					currentPiece = 0;
				} else {
					currentPiece++;
				}
				nextTrackPiece = getNextTrackPiece(currentPiece);
			}
			if (nextTrackPiece.getAngle() < 0) {
				return Turn.LEFT;
			} else if (nextTrackPiece.getAngle() > 0) {
				return Turn.RIGHT;
			} else {
				while (nextTrackPiece.getAngle() == 0.0) {
					if (currentPiece == race.getTrack().getPieces().size() - 1) {
						currentPiece = 0;
					} else {
						currentPiece++;
					}
					nextTrackPiece = getNextTrackPiece(currentPiece);
				}
				if (nextTrackPiece.getAngle() < 0) {
					return Turn.LEFT;
				} else {
					return Turn.RIGHT;
				}
			}
		}

	}

	public int getCurrentTrackPieceIndex() {
		return currentCarPositions.get(myCar).getPiecePosition()
				.getPieceIndex();
	}

	public String getPreviousTurn() {
		int currentPiece = currentCarPositions.get(myCar).getPiecePosition()
				.getPieceIndex();
		TrackPiece nextTrackPiece = getNextTrackPiece(currentPiece);
		while (nextTrackPiece.getAngle() == 0.0) {
			if (currentPiece == 0) {
				currentPiece = race.getTrack().getPieces().size() - 1;
			} else {
				currentPiece--;
			}
			nextTrackPiece = getNextTrackPiece(currentPiece);
		}
		if (nextTrackPiece.getAngle() < 0.0) {
			return "Left";
		} else {
			return "Right";
		}
	}

	private TrackPiece getNextTrackPiece(int currentPieceIdx) {
		if (currentPieceIdx == race.getTrack().getPieces().size() - 1) {
			return race.getTrack().getPieces().get(0);
		}
		return race.getTrack().getPieces().get(currentPieceIdx + 1);
	}

	public boolean isOnStraightPiece() {
		return race
				.getTrack()
				.getPieces()
				.get(currentCarPositions.get(myCar).getPiecePosition()
						.getPieceIndex()).getTurn() == Turn.NO_TURN;
	}

	public double getDistanceTillTurn() {
		double toReturn = 0;
		double distanceFromCenter = race
				.getTrack()
				.getLaneByIndex(
						currentCarPositions.get(myCar).getPiecePosition()
								.getLane().getEndLaneIndex())
				.getDistanceFromCenter();
		if (isOnStraightPiece()) {
			toReturn = race
					.getTrack()
					.getPieces()
					.get(currentCarPositions.get(myCar).getPiecePosition()
							.getPieceIndex()).getLength(distanceFromCenter)
					- currentCarPositions.get(myCar).getPiecePosition()
							.getInPieceDistance();
			int pieceIdx = getCurrentTrackPieceIndex();
			if (pieceIdx == race.getTrack().getPieces().size() - 1)
				pieceIdx = 0;
			TrackPiece nextTrackPiece = getNextTrackPiece(pieceIdx);
			while (nextTrackPiece.getTurn() == Turn.NO_TURN) {
				toReturn += nextTrackPiece.getLength(distanceFromCenter);
				pieceIdx++;
				if (pieceIdx == race.getTrack().getPieces().size() - 1) {
					pieceIdx = 0;
				}
				nextTrackPiece = getNextTrackPiece(pieceIdx);
			}
		}
		return toReturn;
	}

	public void setTurbo(Turbo turbo) {
		this.turbo = turbo;
	}

	public boolean isTurboAvailable() {
		return turbo != null;
	}

	public Turbo getTurbo() {
		return turbo;
	}

	public double getAngleIncrementSpeed() {
		// TODO: check if works correct with negative angles.
		if (currentCarPositions.get(myCar) == null)
			return 0.0;
		if (previousCarPositions.get(myCar) == null)
			return currentCarPositions.get(myCar).getAngle();
		return currentCarPositions.get(myCar).getAngle()
				- previousCarPositions.get(myCar).getAngle();
	}

	public double getPreviousSpeed() {
		return previousSpeed;
	}

	public void setPreviousSpeed(double previousSpeed) {
		this.previousSpeed = previousSpeed;
	}

	public double getAcceleration() {
		return currentSpeed() - previousSpeed;
	}

	public double getNextSegmentLength(Turn direction) {// Segment is track part
														// between switches.
		int pieceIdx = getCurrentTrackPieceIndex();
		TrackPiece nextTrackPiece = getNextTrackPiece(pieceIdx);
		while (!nextTrackPiece.isSwitch()) {
			pieceIdx++;
			if (pieceIdx == race.getTrack().getPieces().size())
				pieceIdx = 0;
			nextTrackPiece = getNextTrackPiece(pieceIdx);
		}
		pieceIdx++;
		if (pieceIdx == race.getTrack().getPieces().size())
			pieceIdx = 0;
		double toReturn = 0.0;
		double distanceFromCenter = race
				.getTrack()
				.getLaneByIndex(
						currentCarPositions.get(myCar).getPiecePosition()
								.getLane().getEndLaneIndex())
				.getDistanceFromCenter();
		while (!(nextTrackPiece = getNextTrackPiece(pieceIdx)).isSwitch()) {
			if (nextTrackPiece.getTurn() == direction)
				toReturn += nextTrackPiece.getLength(distanceFromCenter);
			pieceIdx++;
			if (pieceIdx == race.getTrack().getPieces().size())
				pieceIdx = 0;
		}
		return toReturn;
	}

	public double getCurrentElementLength() { // Element is one direction turn,
												// straight lane.
		int currentPieceIdx = getCurrentTrackPieceIndex();
		TrackPiece currentTrackPiece = getCurrentTrackPiece();
		TrackPiece nextTrackPiece = getNextTrackPiece(currentPieceIdx);
		double distanceFromCenter = race
				.getTrack()
				.getLaneByIndex(
						currentCarPositions.get(myCar).getPiecePosition()
								.getLane().getEndLaneIndex())
				.getDistanceFromCenter();
		double length = currentTrackPiece.getLength(distanceFromCenter)
				- currentCarPositions.get(myCar).getPiecePosition()
						.getInPieceDistance();
		while (currentTrackPiece.getTurn() == nextTrackPiece.getTurn()) {
			length += nextTrackPiece.getLength(distanceFromCenter);
			currentPieceIdx++;
			if (currentPieceIdx == race.getTrack().getPieces().size())
				currentPieceIdx = 0;
			nextTrackPiece = getNextTrackPiece(currentPieceIdx);
		}
		return length;
	}

    public double getDistanceToNextTurn() {
        PiecePosition piecePosition =  currentCarPositions.get(myCar).getPiecePosition();
        int myPieceNdx =piecePosition.getPieceIndex();
        int nextTurnNdx = getNextTurnIndex();
        return getMinDistanceBetween(myPieceNdx, nextTurnNdx) - piecePosition.getInPieceDistance();
    }

    public double getMinDistanceBetween(int index1, int index2) { // [ index1, index2 )
        Track track = race.getTrack();
        List<TrackPiece> trackPieces = track.getPieces();
        int currentIndex = index1;
        double ret = 0;
        TrackPiece currentTrackPiece;
        while (currentIndex != index2) {
            currentTrackPiece = trackPieces.get(currentIndex);
            if (currentTrackPiece.getTurn() == Turn.NO_TURN) {
                ret += currentTrackPiece.getLength();
            } else if (currentTrackPiece.getTurn() == Turn.RIGHT) {
                int rightmostLane = track.getRightmostLane();
                Lane lane = track.getLaneByIndex(rightmostLane);
                ret += currentTrackPiece.getLength(lane.getDistanceFromCenter());
            } else if (currentTrackPiece.getTurn() == Turn.LEFT) {
                int leftmostLane = track.getLeftmostLane();
                Lane lane = track.getLaneByIndex(leftmostLane);
                ret += currentTrackPiece.getLength(lane.getDistanceFromCenter());
            }
            currentIndex ++;
            if (currentIndex >= trackPieces.size()) {
                currentIndex = 0;
            }
            if (currentIndex == index1) {
                // emergency cycle break, index2 is unreachable
                ret = -1;
                break;
            }
        }
        return  ret;
    }

    public int getNextTurnIndex() {
        List<TrackPiece> trackPieces = race.getTrack().getPieces();
        int currentIndex = currentCarPositions.get(myCar).getPiecePosition().getPieceIndex();
        TrackPiece trackPiece = trackPieces.get(currentIndex);
        double radius = trackPiece.getRadius();
        double angle = trackPiece.getAngle();
        currentIndex ++;
        if (currentIndex >= trackPieces.size()) {
            currentIndex = 0;
        }
        TrackPiece nextTrackPiece = trackPieces.get(currentIndex);
        while (nextTrackPiece.getTurn() == Turn.NO_TURN || (nextTrackPiece.getAngle() == angle && nextTrackPiece.getRadius() == radius)) {
            radius = nextTrackPiece.getRadius();
            angle = nextTrackPiece.getAngle();
            currentIndex ++;
            if (currentIndex >= trackPieces.size()) {
                currentIndex = 0;
            }
            nextTrackPiece = trackPieces.get(currentIndex);
        }
        return currentIndex;
    }

	public double getAngleIncrementAcceleration() {
		return getAngleIncrementSpeed() - previousAngleSpeed;
	}

	public boolean isNextTrackPieceSwitch() {
		return getNextTrackPiece(getCurrentTrackPieceIndex()).isSwitch();
	}

	public ArrayList<CarId> getCarsOnTheSamePieceInfrontOfMe() {
		ArrayList<CarId> toReturn = new ArrayList<CarId>();
		LanePosition myLane = currentCarPositions.get(myCar).getPiecePosition()
				.getLane();
		int myPieceIdx = currentCarPositions.get(myCar).getPiecePosition()
				.getPieceIndex();
		double myInPieceDistance = currentCarPositions.get(myCar)
				.getPiecePosition().getInPieceDistance();
		int myLap = currentCarPositions.get(myCar).getPiecePosition().getLap();
		for (CarId carId : currentCarPositions.keySet()) {
			if (carId == myCar)
				continue;
			if (currentCarPositions.get(carId).getPiecePosition().getLane() == myLane) {
				if (currentCarPositions.get(carId).getPiecePosition()
						.getPieceIndex() == myPieceIdx) {
					if (myInPieceDistance < currentCarPositions.get(carId)
							.getPiecePosition().getInPieceDistance()) {
						toReturn.add(carId);
					}
				} else if (currentCarPositions.get(carId).getPiecePosition()
						.getPieceIndex() > myPieceIdx) {
					toReturn.add(carId);
				} else {
					if (currentCarPositions.get(carId).getPiecePosition()
							.getLap() > myLap) {
						toReturn.add(carId);
					}
				}

			}
		}
		return toReturn;
	}

	public double getCurrentDistanceToCar(CarId carId) {
		// TODO Auto-generated method stub
		double myPosition = currentCarPositions.get(myCar).getPiecePosition()
				.getInPieceDistance();
		if (getCurrentTrackPieceIndex() == currentCarPositions.get(carId)
				.getPiecePosition().getPieceIndex()) {
			return currentCarPositions.get(carId).getPiecePosition()
					.getInPieceDistance()
					- myPosition;
		} else {
			double toReturn = getCurrentTrackPiece().getLength(
					race.getTrack()
							.getLaneByIndex(
									currentCarPositions.get(myCar)
											.getPiecePosition().getLane()
											.getEndLaneIndex())
							.getDistanceFromCenter())
					- myPosition;
			int currentPieceIdx = getCurrentTrackPieceIndex() + 1;
			if (currentPieceIdx == race.getTrack().getPieces().size())
				currentPieceIdx = 0;
			int switchesCount = 0;
			while (currentPieceIdx != currentCarPositions.get(carId)
					.getPiecePosition().getPieceIndex()) {
				if (race.getTrack().getPieces().get(currentPieceIdx).isSwitch())
					switchesCount++;

				if (switchesCount == 2)
					return Double.MAX_VALUE; // car is 2 switches away from us,
												// no need to overtake anyways.

				if (switchesCount == 1) {
					if (currentCarPositions.get(myCar).getPiecePosition()
							.getLane().getEndLaneIndex() == getOptimalLaneForPiece(currentPieceIdx)) {

					}
				}
			}
			return toReturn;
		}
	}

	private int getOptimalLaneForPiece(int currentPieceIdx) {
		double lengthOnLeftLane = 0.0;
		double lengthOnRightLane = 0.0;
		int idx = currentPieceIdx;
		TrackPiece nextPiecePosition = getNextTrackPiece(idx);
		while (!nextPiecePosition.isSwitch()) {
			idx++;
			if (idx == race.getTrack().getPieces().size())
				idx = 0;

			lengthOnLeftLane = nextPiecePosition.getLength(race.getTrack()
					.getLeftmostLane());
			lengthOnRightLane = nextPiecePosition.getLength(race.getTrack()
					.getRightmostLane());

			nextPiecePosition = getNextTrackPiece(idx);
		}
		idx = currentPieceIdx;
		TrackPiece previousPiecePosition = getPreviousTrackPiece(idx);
		while (!previousPiecePosition.isSwitch()) {
			idx--;
			if (idx == -1)
				idx = race.getTrack().getPieces().size() - 1;

			lengthOnLeftLane = previousPiecePosition.getLength(race.getTrack()
					.getLeftmostLane());
			lengthOnRightLane = previousPiecePosition.getLength(race.getTrack()
					.getRightmostLane());

			previousPiecePosition = getPreviousTrackPiece(idx);
		}
		if (lengthOnLeftLane > lengthOnRightLane)
			return race.getTrack().getRightmostLane();
		else
			return race.getTrack().getLeftmostLane();
	}

	private TrackPiece getPreviousTrackPiece(int idx) {
		if (idx == 0)
			return race.getTrack().getPieces()
					.get(race.getTrack().getPieces().size() - 1);
		return race.getTrack().getPieces().get(idx - 1);
	}

	public ArrayList<CarId> getPossibleOvertakingTargets() {
		ArrayList<CarId> toReturn = new ArrayList<CarId>();
		int myCurrentPieceID = getCurrentTrackPieceIndex();
		int secondSwitchID = getSecondSwitchPieceId(myCurrentPieceID);

		for (CarId carId : currentCarPositions.keySet()) {
			if (currentCarPositions.get(carId).getPiecePosition().getLane()
					.getEndLaneIndex() != getOptimalLaneForPiece(currentCarPositions
					.get(carId).getPiecePosition().getPieceIndex()))
				continue;// car is not on optimal lane, who cares about it.
			if (currentCarPositions.get(carId).getPiecePosition()
					.getPieceIndex() == myCurrentPieceID) {
				if (currentCarPositions.get(carId).getPiecePosition()
						.getInPieceDistance() > currentCarPositions.get(myCar)
						.getPiecePosition().getInPieceDistance()) {
					toReturn.add(carId);
				}
			} else if (currentCarPositions.get(carId).getPiecePosition()
					.getPieceIndex() > myCurrentPieceID
					&& currentCarPositions.get(carId).getPiecePosition()
							.getPieceIndex() < secondSwitchID) {
				toReturn.add(carId);
			}
		}
		return toReturn;
	}

	public int getSecondSwitchPieceId(int myCurrentPieceID) {
		int pieceIdx = myCurrentPieceID;
		TrackPiece nextTrackPiece = getNextTrackPiece(pieceIdx);
		int switchCounter = 0;
		while (switchCounter < 2) {
			if (nextTrackPiece.isSwitch())
				switchCounter++;
			pieceIdx++;
			if (pieceIdx == race.getTrack().getPieces().size())
				pieceIdx = 0;
			nextTrackPiece = getNextTrackPiece(pieceIdx);
		}
		return pieceIdx;
	}

	public double getDistance(int endPieceIdx, boolean useOptimalLane,
			CarId carId) {
		int currentPieceLane = currentCarPositions.get(carId)
				.getPiecePosition().getLane().getEndLaneIndex();
		int currentPieceIdx = currentCarPositions.get(carId).getPiecePosition()
				.getPieceIndex();
		double toRet = race
				.getTrack()
				.getPieces()
				.get(currentCarPositions.get(carId).getPiecePosition()
						.getPieceIndex())
				.getLength(
						race.getTrack().getLaneByIndex(currentPieceLane)
								.getDistanceFromCenter())
				- currentCarPositions.get(carId).getPiecePosition()
						.getInPieceDistance();
		while (currentPieceIdx <= endPieceIdx) {
			currentPieceIdx++;
			if (currentPieceIdx == race.getTrack().getPieces().size())
				currentPieceIdx = 0;
			if (race.getTrack().getPieces().get(currentPieceIdx).isSwitch()) {
				if (useOptimalLane)
					currentPieceLane = getOptimalLaneForPiece(currentPieceIdx);
				else
					currentPieceLane = getShortestNotOptimalLaneForPiece(currentPieceIdx);
			}// switching to other lane if needed
			toRet += race
					.getTrack()
					.getPieces()
					.get(currentPieceIdx)
					.getLength(
							(race.getTrack().getLaneByIndex(currentPieceLane)
									.getDistanceFromCenter()));
		}
		return toRet;
	}

	private int getShortestNotOptimalLaneForPiece(int currentPieceIdx) {
		int optimalLane = getOptimalLaneForPiece(currentPieceIdx);
		List<Lane> lanes = race.getTrack().getLanes();
		double distFromCenter = Double.MAX_VALUE;
		int toRet = 0;
		for (Lane lane : lanes) {
			if(lane.getIndex() == optimalLane) continue;
			if(lane.getDistanceFromCenter()<distFromCenter){
				distFromCenter=lane.getDistanceFromCenter();
				toRet=lane.getIndex();
			}
		}
		return toRet;
	}

	public Turn getTurnForShortestNonOptimalLane(int pieceIdx) {
		int shortestNonOptimal = getShortestNotOptimalLaneForPiece(pieceIdx);
		int currentLane = currentCarPositions.get(myCar).getPiecePosition().getLane().getEndLaneIndex();
		if(race.getTrack().getLaneByIndex(shortestNonOptimal).getDistanceFromCenter()>race.getTrack().getLaneByIndex(currentLane).getDistanceFromCenter()){
			return Turn.LEFT;
		}else if(race.getTrack().getLaneByIndex(shortestNonOptimal).getDistanceFromCenter()<race.getTrack().getLaneByIndex(currentLane).getDistanceFromCenter()){
			return Turn.RIGHT;
		}
		return Turn.NO_TURN;
	}

	public int getClosestPieceIdxAfterSwitch(CarId carId) {
		int currentIdx = currentCarPositions.get(carId).getPiecePosition().getPieceIndex();
		while(!race.getTrack().getPieces().get(currentIdx).isSwitch()){
			currentIdx++;
			if(currentIdx==race.getTrack().getPieces().size()) currentIdx=0;
		}
		currentIdx++;
		if(currentIdx==race.getTrack().getPieces().size()) currentIdx=0;
		return currentIdx;
	}

}
