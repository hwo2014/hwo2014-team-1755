package domen;

/**
 * Created by User1 on 21/04/2014.
 */
public enum Turn {
    NO_TURN(""),
    LEFT("Left"),
    RIGHT("Right");

    private final String data;
    private Turn(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }
}
