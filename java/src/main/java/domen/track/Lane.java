package domen.track;

/**
 * Created by User1 on 16/04/2014.
 */
public class Lane {
    private final int distanceFromCenter;
    private final int index;

    public Lane(final int distanceFromCenter, final int index) {
        this.distanceFromCenter = distanceFromCenter;
        this.index = index;
    }

    @Override
    public String toString() {
        return "distanceFromCenter="+distanceFromCenter+";index="+index;
    }

	public int getIndex() { return index; }

    public int getDistanceFromCenter() { return  distanceFromCenter;}
}
