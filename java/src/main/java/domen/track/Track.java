package domen.track;

import java.util.List;

/**
 * Created by User1 on 16/04/2014.
 */
public class Track {
    private final String id;
    private final String name;
    private final List<TrackPiece> pieces;
    private final List<Lane> lanes;
    private final Object startingPoint; // not used

    // lazy init
    private int leftmostLane;
    private int rightmostLane;

    public Track() {
        this.id = null;
        this.name = null;
        this.pieces = null;
        this.lanes = null;
        this.startingPoint = null;
        this.leftmostLane = Integer.MIN_VALUE;
        this.rightmostLane = Integer.MIN_VALUE;
    }

    public Track(final String id, final String name, final List<TrackPiece> pieces, final List<Lane> lanes, final Object startingPoint) {
        this.id = id;
        this.name = name;
        this.pieces = pieces;
        this.lanes = lanes;
        this.startingPoint = startingPoint;
        this.leftmostLane = Integer.MIN_VALUE;
        this.rightmostLane = Integer.MIN_VALUE;
    }

    @Override
    public String toString() {
        return "id="+id+";name="+name+";pieces="+pieces.toString()+";lanes="+lanes.toString()+";startingPoint="+startingPoint.toString();
    }

	public int getLeftmostLane() {
        if (leftmostLane == Integer.MIN_VALUE) {
            int currentDistance = Integer.MAX_VALUE;
            for(Lane lane : lanes) {
                if (lane.getDistanceFromCenter() < currentDistance ) {
                    currentDistance = lane.getDistanceFromCenter();
                    leftmostLane = lane.getIndex();
                }
            }
        }
        return leftmostLane;
    }

    public Lane getLaneByIndex(int index) {
        for(Lane lane : lanes) {
           if (lane.getIndex() == index) {
               return lane;
           }
        }
        return null;
    }

    public int getRightmostLane() {
        if (rightmostLane == Integer.MIN_VALUE) {
            int currentDistance = Integer.MIN_VALUE;
            for(Lane lane : lanes) {
                if (lane.getDistanceFromCenter() > currentDistance ) {
                    currentDistance = lane.getDistanceFromCenter();
                    rightmostLane = lane.getIndex();
                }
            }
        }
        return rightmostLane;
    }

	public List<TrackPiece> getPieces() {
		return pieces;
	}

	public List<Lane> getLanes() {
		return lanes;
	}
}
