package domen.track;

import com.google.gson.annotations.SerializedName;
import domen.Turn;

import java.util.List;

/**
 * Created by User1 on 16/04/2014.
 */
public class TrackPiece {
    private final double length;
    @SerializedName("switch")
    private final boolean isSwitch;
    private final double radius;
    private final double angle;

    public TrackPiece(final double length, final boolean isSwitch, final double radius, final double angle) {
        this.length = length;
        this.isSwitch = isSwitch;
        this.radius = radius;
        this.angle = angle;
    }

    @Override
    public String toString() {
        return "length="+length+";switch="+isSwitch+";radius="+radius+";angle="+angle;
    }

	public double getAngle() {
		return angle;
	}

    public Turn getTurn() {
        return angle == 0 ? Turn.NO_TURN : angle > 0 ? Turn.RIGHT : Turn.LEFT;
    }
	public double getLength() {
		return getLength(0);
	}

    public double getSwitchingLength(double distanceBetweenLanes) {
        return Math.sqrt(length * length + distanceBetweenLanes * distanceBetweenLanes);
    }

    public double getLength(double distanceFormCenter) {
        if(getTurn() == Turn.NO_TURN) {
            return length;
        } else {
            /* *
             *  angle = -45 -> left turn
             *  angle = 45 -> right turn
             *  distanceFormCenter = -10 -> left form center
             *  distanceFormCenter = 10 -> right form center
             * */
             if (angle > 0) { // right turn, right lane -> shorter radius
                 return Math.PI * 2 * (radius - distanceFormCenter) *angle / 360;
             } else { // left turn, right lane -> longer radius
                 return Math.PI * 2 * (radius + distanceFormCenter) * (-angle) / 360;
             }
        }
    }

    public boolean isSameTurn(double angle, double radius) {
        return angle == this.angle && radius == this.radius;
    }

	public boolean isSwitch() {
		return isSwitch;
	}

	public double getRadius() {
		return radius;
	}
}
