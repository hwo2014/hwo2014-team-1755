package domen;

/**
 * Created by User1 on 16/04/2014.
 */
public class RaceWrapper {
    private final Race race;
    public RaceWrapper(final Race race) {
        this.race = race;
    }

    @Override
    public String toString() {
        return "race="+race.toString();
    }

    public Race getRace() {
        return race;
    }
}
