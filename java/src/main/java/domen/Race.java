package domen;

import domen.car.Car;
import domen.track.Track;

import java.util.List;

/**
 * Created by User1 on 16/04/2014.
 */
public class Race {
    private final Track track;
    private final List<Car> cars;
    private final RaceSession raceSession;

    public Race(final Track track, final List<Car> cars, final RaceSession raceSession) {
        this.track = track;
        this.cars = cars;
        this.raceSession = raceSession;
    }

    @Override
    public String toString() {
        return "track=["+track.toString()+"];cars="+cars.toString()+";raceSession=["+raceSession.toString()+"]";
    }

	public Track getTrack() {
		return track;
	}
}
