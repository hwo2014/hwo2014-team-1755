package domen.car;

/**
 * Created by User1 on 16/04/2014.
 */
public class Car {
    private final CarId id;
    private final CarDimension dimensions;

    public Car(final CarId id, final CarDimension dimensions) {
        this.id = id;
        this.dimensions = dimensions;
    }

    @Override
    public String toString() {
        return "id=["+id.toString()+"];dimensions=["+dimensions.toString()+"]";
    }
}
