package domen.car;

public class Turbo {
	private final double turboDurationMilliseconds;
	private final int turboDurationTicks;
	private final double turboFactor;
	public Turbo(final double turboDurationMilliseconds, final int turboDurationTicks, final double turboFactor){
		this.turboDurationMilliseconds = turboDurationMilliseconds;
		this.turboDurationTicks = turboDurationTicks;
		this.turboFactor = turboFactor;
	}
	@Override
	public String toString() {
		return "Turbo [turboDurationMilliseconds=" + turboDurationMilliseconds
				+ ", turboDurationTicks=" + turboDurationTicks
				+ ", turboFactor=" + turboFactor + "]";
	}
	public int getTurboDurationTicks() {
		return turboDurationTicks;
	}
	public double getTurboFactor() {
		return turboFactor;
	}
}
