package domen.car;

/**
 * Created by User1 on 16/04/2014.
 */
public class CarDimension {
    private final int length;
    private final int width;
    private final int guideFlagPosition;

    public CarDimension(final int length, final int width, final int guideFlagPosition) {
        this.length = length;
        this.width = width;
        this.guideFlagPosition = guideFlagPosition;
    }

    @Override
    public String toString() {
        return "length="+length+";width="+width+";guideFlagPosition="+guideFlagPosition;
    }
}
