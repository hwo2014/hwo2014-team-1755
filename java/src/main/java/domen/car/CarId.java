package domen.car;

public class CarId {
    private static final int BASE_PRIME = 31;

    private final String name;
    private final String color;

    public CarId(final String name, final String color) {
        this.name = name;
        this.color = color;
    }

    @Override
    public String toString() {
        return "name="+name+";color="+color;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (CarId.class != obj.getClass()) {
            return false;
        }
        CarId carId = (CarId) obj;
        return carId.color.equals(color) && carId.name.equals(name);
    }

    @Override
    public int hashCode() {
        return BASE_PRIME * name.hashCode() + color.hashCode();
    }
}
